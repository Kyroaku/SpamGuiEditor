#pragma once

#include <stdio.h>

#include <SDL/SDL.h>

class Fps
{
	int fps, counter, fpsMax;
	float time;

public:
	Fps();
	bool Update(float dt);
	int GetFps();
	void SetFpsMax(int max);
	int GetFpsMax();
};