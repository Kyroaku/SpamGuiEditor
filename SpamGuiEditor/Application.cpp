#include "Application.h"

/* TODO */
/*
####################################################################################
################################# Creating layouts #################################
####################################################################################

#

####################################################################################
##################################### SpamGui ######################################
####################################################################################

### GuiFileDialog
* Implement copy constructor
* Implement serialization and factory

## Add GuiScrollBar
* Up/Down buttons
* scroll button
* dragging scroll button causes setting scroll bar value from min to max

## GuiTreeView
* Add clipArea to cut nodes out of layout
* Optimize closed nodes
* Add scroll bar

## GuiManager
* Fix layouts viewports
* Optimize layouts rendering
- Don't render layouts out of clipArea or out of screen

# GuiWindow
* Add area to drag and move window
* Add setting clipArea option

# GuiEditBox
* Add clipArea to cut letters
* Add handling big letters
* Add carret
* Add moving carret

# Add check box layout
* Subclass of GuiButton (?)

* Add radio buttons layout

####################################################################################
#################################### Editor GUI ####################################
####################################################################################

# Parameters window (position, size, label, ...)

####################################################################################
####################################### App ########################################
####################################################################################

# add error checking in 'CreateWindow' method

*/

Application::Application()
	: mSdlWindow(0)
	, mSdlRenderer(0)
	, mRunning(true)
	, mNewLayout(0)
	, mEditMode(EEditMode::eEditSelect)
{
	SDL_Init(SDL_INIT_EVERYTHING);

	CreateWindow("SpamGUI Editor", 1024, 768, false);

	gui.init(new GuiRendererSDL(GetSDLWindow(), GetSDLRenderer()));
	int w, h;
	SDL_GetWindowSize(GetSDLWindow(), &w, &h);
	gui.resize(vec2(w, h));

	gui.registerFactory("GuiLayout", new GuiFactory<GuiLayout>);
	gui.registerFactory("GuiWindow", new GuiFactory<GuiWindow>);
	gui.registerFactory("GuiButton", new GuiFactory<GuiButton>);
	gui.registerFactory("GuiText", new GuiFactory<GuiText>);
	gui.registerFactory("GuiRefButton", new GuiFactory<GuiRefButton>);
	gui.registerFactory("GuiEditBox", new GuiFactory<GuiEditBox>);
	gui.registerFactory("GuiTreeView", new GuiFactory<GuiTreeView>);
	gui.registerFactory("GuiScrollBar", new GuiFactory<GuiScrollBar>);
	gui.registerFactory("GuiShopDialog", new GuiFactory<GuiShopDialog>);
	gui.registerFactory("GuiCheckBox", new GuiFactory<GuiCheckBox>);
	gui.registerFactory("GuiRadioButton", new GuiFactory<GuiRadioButton>);

	mFpsText = (GuiText*)gui.addLayout(new GuiText(
		gui.getFont("Data/Fonts/Arial_14.bff"),
		"Fps: 0",
		vec2(0.0f)));
	mFpsText->setAlign(GuiLayout::AlignRight, GuiLayout::AlignTop);
	mFpsText->setAdjustMode(eAdjustMode::Vertical);
	mFpsText->setSize(glm::vec2(60.0f, mFpsText->getSize().y));
#ifdef _DEBUG
	mFps.SetFpsMax(0);
#else
	mFps.SetFpsMax(60);
#endif

	gui.addLayout(mRootLayout = new GuiLayout(vec2(0.0f, 0.0f), gui.getRootLayout()->getSize() - vec2(0.0f, 0.0f)));
	mRootLayout->setLabel("mroot_layout");
	mRootLayout->setFocusable(false);
	gui.addLayout(mGuiLayout["save"] = new GuiButton("Save", vec2(0.0f, 0.0f), vec2(70.0f, 20.0f)));
	gui.addLayout(mGuiLayout["load"] = new GuiButton("Load", vec2(70.0f, 0.0f), vec2(70.0f, 20.0f)));
	gui.addLayout(mGuiLayout["new_window"] = new GuiButton("Window", vec2(0.0f, 20.0f), vec2(140.0f, 30.0f)));
	gui.addLayout(mGuiLayout["new_button"] = new GuiButton("Button", vec2(0.0f, 50.0f), vec2(140.0f, 30.0f)));
	gui.addLayout(mGuiLayout["new_editbox"] = new GuiButton("Editbox", vec2(0.0f, 80.0f), vec2(140.0f, 30.0f)));
	gui.addLayout(mGuiLayout["new_treeview"] = new GuiButton("TreeView", vec2(0.0f, 110.0f), vec2(140.0f, 30.0f)));
	gui.addLayout(mGuiLayout["new_scrollbar"] = new GuiButton("ScrollBar", vec2(0.0f, 140.0f), vec2(140.0f, 30.0f)));
	gui.addLayout(mGuiLayout["new_checkbox"] = new GuiButton("CheckBox", vec2(0.0f, 170.0f), vec2(140.0f, 30.0f)));
	gui.addLayout(mGuiLayout["new_radiobutton"] = new GuiButton("RadioButton", vec2(0.0f, 200.0f), vec2(140.0f, 30.0f)));
	gui.addLayout(mGuiLayout["example"] = new GuiButton("Example", vec2(0.0f, 230.0f), vec2(140.0f, 30.0f)));
	gui.addLayout(mGuiLayout["layout_tree"] = new GuiTreeView(vec2(0.0f, 260.0f), vec2(140.0f, 300.0f)));
	GuiTreeViewCast(mGuiLayout["layout_tree"])->addNode(new GuiTreeViewLayoutNode(mRootLayout))->setOpen(true);

	gui.print(gui.getRootLayout());

#ifdef _DEBUG
	gui.setMode(SGUI_MODE_EDIT_MODE | SGUI_MODE_SHOW_SIZE);
#else
	//gui.setMode(SGUI_MODE_EDIT_MODE /* | SGUI_MODE_SHOW_SIZE*/);
#endif
}
Application::~Application()
{
	if (mSdlRenderer) SDL_DestroyRenderer(mSdlRenderer);
	if (mSdlWindow) SDL_DestroyWindow(mSdlWindow);
}

void Application::HandleEvent(SDL_Event &event)
{
	gui.onInput();

	/* If new layout has been just created */
	if (mNewLayout != 0)
	{
		/* First click - set position */
		if (input.isButtonDown(SDL_BUTTON_LEFT)) {
			GuiLayout *parent = 0;
			if (gui.getFocusLayout())
				if (gui.getFocusLayout()->isEditable())
					parent = gui.getFocusLayout();
				else
					parent = mRootLayout;
			parent->addLayout(mNewLayout);
			GuiTreeViewNode *node = GuiTreeViewCast(mGuiLayout["layout_tree"])->getNode(0);
			node = GuiTreeViewLayoutCast(node)->findByLayout(parent);
			GuiTreeViewLayoutCast(node)->addNode(mNewLayout)->setOpen(true);
			mNewLayout->setAbsolutePosition(input.mouse);
			mNewLayout->setEnabled(true);
			mNewLayout->setSelected(true);
			mNewLayout->setEditable(true);
			mEditMode = eEditResize;
		}
		if (mEditMode != eEditCreate)
		{
			/* Drag - set size */
			if (input.isButton(SDL_BUTTON_LEFT))
				mNewLayout->setSize(input.mouse - mNewLayout->getAbsolutePosition());
			/* End creating layout */
			if (input.isButtonUp(SDL_BUTTON_LEFT))
				mNewLayout = 0;
		}
	}

	if (input.isKeyDown(SDL_SCANCODE_SPACE))
	{
		system("cls");
		gui.print(gui.getRootLayout());
		if (gui.getFocusLayout())
			printf("\nFocus: %s (%p)", gui.getFocusLayout()->getLabel().c_str(), gui.getFocusLayout());
	}

	if (GuiButtonCast(mGuiLayout["new_window"])->isClicked())
	{
		mNewLayout = new GuiWindow();
		mNewLayout->setEnabled(false);
		mEditMode = EEditMode::eEditCreate;
	}
	if (GuiButtonCast(mGuiLayout["new_button"])->isClicked())
	{
		mNewLayout = new GuiButton();
		mNewLayout->setEnabled(false);
		mEditMode = EEditMode::eEditCreate;
	}
	if (GuiButtonCast(mGuiLayout["new_editbox"])->isClicked())
	{
		mNewLayout = new GuiEditBox();
		mNewLayout->setEnabled(false);
		mEditMode = EEditMode::eEditCreate;
	}
	if (GuiButtonCast(mGuiLayout["new_treeview"])->isClicked())
	{
		mNewLayout = new GuiTreeView();
		mNewLayout->setEnabled(false);
		mEditMode = EEditMode::eEditCreate;
	}
	if (GuiButtonCast(mGuiLayout["new_scrollbar"])->isClicked())
	{
		mNewLayout = new GuiScrollBar();
		mNewLayout->setEnabled(false);
		mEditMode = EEditMode::eEditCreate;
	}
	if (GuiButtonCast(mGuiLayout["new_checkbox"])->isClicked())
	{
		mNewLayout = new GuiCheckBox();
		mNewLayout->setEnabled(false);
		mEditMode = EEditMode::eEditCreate;
	}
	if (GuiButtonCast(mGuiLayout["new_radiobutton"])->isClicked())
	{
		mNewLayout = new GuiRadioButton();
		mNewLayout->setEnabled(false);
		mEditMode = EEditMode::eEditCreate;
	}

	if (GuiButtonCast(mGuiLayout["example"])->isClicked())
	{
		mRootLayout->addLayout(new GuiButton("Button", glm::vec2(140.0f, 0.0f), glm::vec2(200.0f, 30.0f)))->setLabel("TestButton");
		mRootLayout->addLayout(new GuiWindow(glm::vec2(140.0f, 30.0f), glm::vec2(200.0f, 200.0f)));
		mRootLayout->addLayout(new GuiEditBox(glm::vec2(140.0f, 230.0f), glm::vec2(200.0f, 30)));
		GuiTreeView *tree = 0;
		tree = (GuiTreeView*)mRootLayout->addLayout(new GuiTreeView(glm::vec2(140.0f, 260.0f), glm::vec2(200.0f, 300)));
		tree->addNode("Node 1");
		tree->getNode(0)->addNode("Node 11");
		tree->getNode(0)->getNode(0)->addNode("Node 111");
		tree->getNode(0)->addNode("Node 12");
		tree->addNode("Node 2");
		tree->getNode(1)->addNode("Node 21");
		tree->getNode(1)->getNode(0)->addNode("Node 211");
		tree->getNode(1)->getNode(0)->addNode("Node 212");
		tree->getNode(1)->addNode("Node 22");
		tree->addNode("Node 3");
		tree->getNode(2)->addNode("Node 31");
		tree->getNode(2)->addNode("Node 32");
		tree->getNode(2)->getNode(1)->addNode("Node 321");
		tree->getNode(2)->getNode(1)->addNode("Node 322");
		tree->getNode(2)->addNode("Node 33");
	}

	if (gui.getRootLayout()->find("TestButton"))
		if (GuiButtonCast(gui.getRootLayout()->find("TestButton"))->isClicked())
			printf("CLICKED !\n");

	if (GuiButtonCast(mGuiLayout["save"])->isClicked())
	{
		if (!fileDialog)
		{
			fileDialog = (GuiFileDialog*)gui.addLayout(new GuiFileDialog());
			fileDialog->setLabel("save_file");
			fileDialog->getTitle()->setText("Save as");
			fileDialog->getFileButton()->getText()->setText("Save");
		}
	}
	if (GuiButtonCast(mGuiLayout["load"])->isClicked())
	{
		if (!fileDialog)
		{
			fileDialog = (GuiFileDialog*)gui.addLayout(new GuiFileDialog());
			fileDialog->setLabel("open_file");
			fileDialog->getTitle()->setText("Open file");
			fileDialog->getFileButton()->getText()->setText("Open");
		}
	}
}
void Application::Update(float dt)
{
	gui.onUpdate(dt);

	if (fileDialog)
		if (!fileDialog->isDestroyed())
		{
			std::vector<std::string> pathes = fileDialog->getPathes();
			for (unsigned int i = 0; i < pathes.size(); i++)
			{
				if (fileDialog->getLabel() == "save_file")
				{
					gui.save(pathes.at(i), mRootLayout);
				}
				else
					if (fileDialog->getLabel() == "open_file")
					{
						GuiLayout *layout = mRootLayout->addLayout(gui.load(pathes.at(i)));
						if (layout)
							layout->setEditable(true);
					}
			}
			if (!pathes.empty()) {
				fileDialog->destroy();
				fileDialog = 0;
			}
		}
		else
			fileDialog = 0;
}
void Application::Render()
{
	gui.onDraw();
}

void Application::Loop()
{
	SDL_Event event;
	Uint32 time = SDL_GetTicks();
	float dt = 0.0f;

	while (IsRunning())
	{
		time = SDL_GetTicks();

		event.type = 0;
		input.event.type = 0;
		input.mouseRel = glm::vec2(0.0f);
		/* Input events */
		while (SDL_PollEvent(&event))
		{
			input.onEvent(event);

			switch (event.type)
			{
			case SDL_QUIT:
				Quit();
				break;
			}

			HandleEvent(event);
		}
		/* Update application */
		Update(dt);
		/* Render application */
		SDL_SetRenderDrawColor(mSdlRenderer, 0, 0, 0, 255);
		SDL_RenderClear(GetSDLRenderer());
		Render();
		SDL_RenderPresent(GetSDLRenderer());

		if (mFps.Update(dt))
		{
			char buff[16];
			sprintf_s(buff, "Fps: %d", mFps.GetFps());
			mFpsText->setText(buff);
			printf("%s\n", buff);
		}
		Uint32 ticks = SDL_GetTicks() - time;
		if (mFps.GetFpsMax() != 0)
			if (ticks < (Uint32)1000 / mFps.GetFpsMax())
				SDL_Delay(1000 / mFps.GetFpsMax() - ticks);
		dt = (SDL_GetTicks() - time) / 1000.0f;
	}
}

bool Application::CreateWindow(string _title, int _width, int _height, bool _fullscreen)
{
	/* Create window and renderer */
	mSdlWindow = SDL_CreateWindow(_title.c_str(), SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, _width, _height, SDL_WINDOW_RESIZABLE | SDL_RENDERER_PRESENTVSYNC);
	mSdlRenderer = SDL_CreateRenderer(mSdlWindow, -1, SDL_RENDERER_ACCELERATED);
	SDL_SetRenderDrawBlendMode(mSdlRenderer, SDL_BLENDMODE_BLEND);

	return true;
}
void Application::Quit()
{
	mRunning = false;
}
bool Application::IsRunning()
{
	return mRunning;
}

SDL_Window *Application::GetSDLWindow()
{
	return mSdlWindow;
}
SDL_Renderer *Application::GetSDLRenderer()
{
	return mSdlRenderer;
}