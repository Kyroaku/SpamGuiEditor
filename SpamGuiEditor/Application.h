#pragma once

#include <SDL/SDL.h>
#include <string>
#include <map>
//#include <boost/filesystem.hpp>

#include "SpamGui/GuiManager.h"
#include "SpamGui/GuiShopDialog.h"

#include "SpamGui/GuiRendererSDL.h"

#include "Fps.h"

#define GuiLayoutCast(a)			((GuiLayout*)a)
#define GuiButtonCast(a)			((GuiButton*)a)
#define GuiWindowCast(a)			((GuiWindow*)a)
#define GuiTextCast(a)				((GuiText*)a)
#define GuiTreeViewCast(a)			((GuiTreeView*)a)
#define GuiTreeViewNodeCast(a)		((GuiTreeViewNode*)a)
#define GuiTreeViewLayoutCast(a)	((GuiTreeViewLayoutNode*)a)
#define GuiEditBoxCast(a)			((GuiEditBox*)a)

#pragma comment(lib, "SDL2")
#pragma comment(lib, "FreeImage")

using namespace glm;
using std::string;
using std::map;

enum EEditMode
{
	eEditSelect = 0,
	eEditMove,
	eEditResize,
	eEditCreate
};

class Application
{
	/* Handle to SDL window */
	SDL_Window *mSdlWindow;
	/* Handle to SDL renderer */
	SDL_Renderer *mSdlRenderer;

	/* 'true' when application is running */
	bool mRunning;

	GuiText *mFpsText;
	Fps mFps;

	map<char*, GuiLayout*>mGuiLayout;
	GuiLayout *mRootLayout;

	EEditMode mEditMode;
	GuiLayout *mNewLayout;
	GuiFileDialog *fileDialog;

public:
	Application();
	~Application();
	
	/* Handles application events */
	void HandleEvent(SDL_Event &event);
	/* Updates application */
	void Update(float dt);
	/* Renders application */
	void Render();
	/* Enters the Real Time Loop */
	void Loop();

	/* Creates window */
	bool CreateWindow(string _title, int _width, int _height, bool _fullscreen);
	/* Leaves real time loop */
	void Quit();
	/* 'true' if application is running, 'false' otherwise */
	bool IsRunning();

	SDL_Window *GetSDLWindow();
	SDL_Renderer *GetSDLRenderer();
};