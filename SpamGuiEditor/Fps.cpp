#include "Fps.h"

Fps::Fps()
: fps(0)
, counter(0)
, time(0.0f)
, fpsMax(0)
{

}
int Fps::GetFps()
{
	return fps;
}
void Fps::SetFpsMax(int max)
{
	fpsMax = max;
}
int Fps::GetFpsMax()
{
	return fpsMax;
}
bool Fps::Update(float dt)
{
	time += dt;
	counter++;
	if (time >= 1.0f)
	{
		time -= 1.0f;
		fps = counter;
		counter = 0;
		return true;
	}
	return false;
}