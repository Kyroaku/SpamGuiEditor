#include "GuiLayout.h"

#include "GuiManager.h"

const float GuiLayout::AlignLeft = 0.0f;
const float GuiLayout::AlignRight = 1.0f;
const float GuiLayout::AlignTop = 0.0f;
const float GuiLayout::AlignBottom = 1.0f;
const float GuiLayout::AlignCenter = 0.5f;

GuiLayout::GuiLayout(glm::vec2 p, glm::vec2 s) :
	label("layout"),
	parent(0),
	position(p),
	size(s),
	align(glm::vec2(AlignLeft, AlignTop)),
	enabled(true),
	destroyed(false),
	removed(false),
	belongsToParent(false),
	savable(true),
	editable(false),
	focusable(true),
	clipped(false)
{
	clipArea = glm::vec4(0.0f);
	setID("GuiLayout");
}
GuiLayout::GuiLayout() :
	GuiLayout(glm::vec2(0.0f), glm::vec2(0.0f))
{

}
GuiLayout::GuiLayout(const GuiLayout& c)
	: label(c.label)
	, parent(c.parent)
	, position(c.position)
	, size(c.size)
	, align(c.align)
	, clipArea(c.clipArea)
	, enabled(c.enabled)
	, removed(c.removed)
	, destroyed(c.destroyed)
	, savable(c.savable)
	, id(c.id)
	, editable(c.editable)
	, focusable(c.focusable)
	, clipped(c.clipped)
{
	for (unsigned int i = 0; i < c.childs.size(); i++)
		if (c.childs[i]->isSavable())
		{
			GuiLayout *layout = gui.getFactory(c.childs[i]->id)->copy(c.childs[i]);
			addLayout(layout);
		}
}
GuiLayout::~GuiLayout()
{
	/* delete all childrens */
	for (unsigned int i = 0; i < getNumChildrens(); i++)
		if (getChildren(i))
			delete getChildren(i);
}

void GuiLayout::onInput()
{

}
void GuiLayout::onUpdate(float dt)
{

}
void GuiLayout::onDraw()
{
	/* Render layout size */
	if (GuiManager::get().getMode() & SGUI_MODE_SHOW_SIZE ||
		(GuiManager::get().getMode() & SGUI_MODE_EDIT_MODE && isEditable() && isSelected()))
	{
		GuiManager::get().getRenderer()->setClipArea(glm::vec2(0.0f), glm::vec2(-1.0f));
		GuiManager::get().getRenderer()->drawRect(getAbsolutePosition() - 1.0f, getSize() + 1.0f, 150, 0, 0);
	}
	if (getParent() && getParent()->isClipped()) {
		glm::vec4 area = getParent()->getAbsoluteClipArea();
		GuiManager::get().getRenderer()->setClipArea(glm::vec2(area.x, area.y), glm::vec2(max(area.z - 1, 0.0f), max(area.w - 1, 0.0f)));
	}
	else {

	}
}

void GuiLayout::setLabel(std::string label)
{
	this->label = label;
}
void GuiLayout::setPosition(glm::vec2 p)
{
	position = p;
}
void GuiLayout::setSize(glm::vec2 s)
{
	size = s;
}
void GuiLayout::setAlign(float h, float v)
{
	align.x = h;
	align.y = v;
}
void GuiLayout::setParent(GuiLayout *p)
{
	parent = p;
}
void GuiLayout::setClipArea(glm::vec4 v)
{
	clipArea = v;
}
void GuiLayout::setEnabled(bool s)
{
	enabled = s;
}
void GuiLayout::setSelected(bool s)
{
	selected = s;
}
void GuiLayout::setRemoved(bool s)
{
	removed = s;
}
void GuiLayout::setDestroyed(bool s)
{
	destroyed = s;
}
void GuiLayout::setBelongsToParent(bool s)
{
	belongsToParent = s;
	setSavable(!s);
}
void GuiLayout::setSavable(bool s)
{
	savable = s;
}
void GuiLayout::setEditable(bool s)
{
	editable = s;
}
void GuiLayout::setFocusable(bool s)
{
	focusable = s;
}
void GuiLayout::setClipped(bool s)
{
	clipped = s;
}
void GuiLayout::setAbsolutePosition(glm::vec2 p)
{
	setPosition(p - getAbsolutePosition() + getPosition());
}
void GuiLayout::setID(std::string id)
{
	this->id = id;
}

std::string GuiLayout::getLabel()
{
	return label;
}
glm::vec2 GuiLayout::getPosition()
{
	return position;
}
glm::vec2 GuiLayout::getSize()
{
	return size;
}
glm::vec2 GuiLayout::getAlign()
{
	return align;
}
GuiLayout *GuiLayout::getParent()
{
	return parent;
}
glm::vec4 GuiLayout::getClipArea()
{
	return clipArea;
}
glm::vec4 GuiLayout::getAbsoluteClipArea()
{
	if (getParent()) {
		glm::vec4 parentClipArea = parent->getAbsoluteClipArea();
		glm::vec2 clipArea = getAbsolutePosition() + glm::vec2(this->clipArea.x, this->clipArea.y);
		float x = max(parentClipArea.x, clipArea.x);
		float y = max(parentClipArea.y, clipArea.y);
		return glm::vec4(
			x,
			y,
			min(parentClipArea.z + parentClipArea.x, clipArea.x + (size.x - this->clipArea.x - this->clipArea[2])) - x,
			min(parentClipArea.w + parentClipArea.y, clipArea.y + (size.y - this->clipArea.y - this->clipArea[3])) - y
		);
	}
	else {
		return glm::vec4(
			position.x + clipArea.x,
			position.y + clipArea.y,
			size.x - clipArea[2],
			size.y - clipArea[3]
		);
	}
}
glm::vec2 GuiLayout::getAbsolutePosition()
{
	glm::vec2 position;
	if (getParent() != 0) {
		position = (getPosition() + getParent()->getSize()*getAlign() + getParent()->getAbsolutePosition() - getSize()*getAlign());
	}
	else {
		position = getPosition() - getSize()*getAlign();
	}
	return position;
}
GuiLayout *GuiLayout::getChildren(int i)
{
	return childs.at(i);
}
unsigned int GuiLayout::getNumChildrens()
{
	return childs.size();
}
std::string GuiLayout::getID()
{
	return id;
}

bool GuiLayout::isEnabled()
{
	return enabled;
}
bool GuiLayout::isDisabled()
{
	return (!enabled || destroyed || removed);
}
bool GuiLayout::isSelected()
{
	return selected;
}
void GuiLayout::remove()
{
	removed = true;
}
bool GuiLayout::isRemoved()
{
	return (removed || destroyed);
}
void GuiLayout::destroy()
{
	destroyed = true;
}
bool GuiLayout::isDestroyed()
{
	return destroyed;
}
bool GuiLayout::isBelongsToParent()
{
	return belongsToParent;
}
bool GuiLayout::isSavable()
{
	return savable;
}
bool GuiLayout::isEditable()
{
	return editable;
}
bool GuiLayout::isFocusable()
{
	return focusable;
}

bool GuiLayout::isClipped()
{
	return clipped;
}

void GuiLayout::move(glm::vec2 d)
{
	position += d;
}
GuiLayout *GuiLayout::find(std::string label)
{
	if (this->label == label)
		return this;
	else
	{
		GuiLayout *layout = 0;
		for (unsigned int i = 0; i < getNumChildrens(); i++)
		{
			if (!getChildren(i))
				continue;
			layout = getChildren(i)->find(label);
			if (layout)
				break;
		}
		return layout;
	}
}
GuiLayout *GuiLayout::addLayout(GuiLayout *layout)
{
	if (layout) {
		layout->setParent(this);
		//layout->setRemoved(false);
		//layout->setDestroyed(false);
	}
	childs.push_back(layout);
	return childs.at(childs.size() - 1);
}
void GuiLayout::removeChildren(int i)
{
	childs.erase(childs.begin() + i);
}
void GuiLayout::removeAllChildrens()
{
	for (unsigned int i = 0; i < childs.size(); i++)
		removeChildren(i);
}

SerializedData GuiLayout::Serialize()
{
	SerializedData data;
	data.Push(label);
	data.Push(position);
	data.Push(size);
	data.Push(align);
	data.Push(clipArea);
	data.Push(savable);
	data.Push(editable);
	data.Push(focusable);
	data.Push(clipped);
	return data;
}

void GuiLayout::Deserialize(SerializedData &data)
{
	data.Pop(label);
	data.Pop(position);
	data.Pop(size);
	data.Pop(align);
	data.Pop(clipArea);
	data.Pop(savable);
	data.Pop(editable);
	data.Pop(focusable);
	data.Pop(clipped);
}