#include "GuiRefButton.h"

#include "GuiManager.h"

GuiRefButton::GuiRefButton(std::string path, GuiTexture *texture, glm::vec2 p, glm::vec2 s) :
GuiButton(texture, "sad", p, s),
refPath(path)
{
	setID("GuiRefButton");
}
GuiRefButton::GuiRefButton(std::string path, glm::vec2 p, glm::vec2 s) :
GuiRefButton(path, getDefaultTexture(), p, s)
{
}

void GuiRefButton::onUpdate(float dt)
{
	if (isClicked())
	if (getParent()->getParent())
	{
		GuiLayout *layout = gui.load(refPath);
		layout->setPosition(getParent()->getPosition());
		getParent()->getParent()->addLayout(layout);
		getParent()->remove();
	}
	else
	{
		getParent()->addLayout(gui.load(refPath));
		this->remove();
	}

	super::onUpdate(dt);
}

void GuiRefButton::setRefPath(std::string path)
{
	this->refPath = path;
}

std::string GuiRefButton::getRefPath()
{
	return this->refPath;
}

SerializedData GuiRefButton::Serialize()
{
	SerializedData data;
	data.Push(super::Serialize());
	data.Push(refPath);
	return data;
}

void GuiRefButton::Deserialize(SerializedData & data)
{
	super::Deserialize(data);
	data.Pop(refPath);
}