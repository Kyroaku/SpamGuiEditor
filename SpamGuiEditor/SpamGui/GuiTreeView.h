#pragma once

#include <vector>

#include "GuiTreeViewNode.h"

class GuiTreeView : public GuiLayout
{
	GuiTexture *texture;

	std::vector<GuiTreeViewNode*>nodes;
	std::vector<GuiTreeViewNode*>selectedNodes;

	bool multiselect;

	void updateNode(GuiTreeViewNode *node, float dt, glm::vec2 &pos);

	static GuiTexture *defaultTexture;

public:
	GuiTreeView(glm::vec2 p = glm::vec2(0.0f), glm::vec2 s = glm::vec2(0.0f));
	GuiTreeView(GuiTreeView &c);
	~GuiTreeView();

	virtual void onInput();
	virtual void onUpdate(float dt);
	virtual void onDraw();

	void setMultiselect(bool s);

	unsigned int getNumNodes();
	std::vector<GuiTreeViewNode*> getSelectedNodes();
	GuiTreeViewNode *getNode(unsigned int i);

	bool isMultiselect();

	GuiTreeViewNode *addNode(GuiTreeViewNode *node);
	GuiTreeViewNode *addNode(std::string text);

	virtual SerializedData Serialize();
	virtual void Deserialize(SerializedData &data);

	static void setDefaultTexture(GuiTexture *t);
	static GuiTexture *getDefaultTexture();
};