#include "GuiScrollBar.h"

#include "GuiManager.h"

GuiButton *GuiScrollBar::defaultButtonUp = 0;
GuiButton *GuiScrollBar::defaultButtonDown = 0;
GuiButton *GuiScrollBar::defaultButtonBar = 0;
GuiTexture *GuiScrollBar::defaultTexture = 0;

GuiScrollBar::GuiScrollBar(glm::vec2 p, glm::vec2 s)
	: GuiLayout(p, s)
	, texture(defaultTexture)
	, progress(0)
	, minProgress(0)
	, maxProgress(10)
	, mouseOffset(glm::vec2(-1.0f))
{
	setID("GuiScrollBar");

	setLabel("scrollbar");

	addLayout(buttonUp = new GuiButton(*defaultButtonUp));
	addLayout(buttonDown = new GuiButton(*defaultButtonDown));
	addLayout(buttonBar = new GuiButton(*defaultButtonBar));
	buttonUp->setBelongsToParent(true);
	buttonBar->setBelongsToParent(true);
	buttonDown->setBelongsToParent(true);

	buttonUp->setSize(glm::vec2(s.x, s.x));
	buttonDown->setPosition(glm::vec2(0.0f, s.y - s.x));
	buttonDown->setSize(glm::vec2(s.x, s.x));
	buttonBar->setPosition(glm::vec2(0.0f, s.x));
	buttonBar->setSize(glm::vec2(s.x, (s.y - 2.0f*s.x) / 2.0f));
}
GuiScrollBar::~GuiScrollBar()
{

}

void GuiScrollBar::onInput()
{

}

void GuiScrollBar::onUpdate(float dt)
{
	if (buttonUp->isClicked()) {
		progress = max(minProgress, progress - 1);
		float p = (float)progress / (maxProgress - minProgress);
		buttonBar->setPosition(glm::vec2(0.0f, p*(size.y - 2 * size.x - buttonBar->getSize().y) + size.x));
	}
	if (buttonDown->isClicked()) {
		progress = min(maxProgress, progress + 1);
		float p = (float)progress / (maxProgress - minProgress);
		buttonBar->setPosition(glm::vec2(0.0f, p*(size.y - 2 * size.x - buttonBar->getSize().y) + size.x));
	}
	if (buttonBar->isPressed()) {
		if (mouseOffset.x < 0.0f) {
			mouseOffset = size.x + input.mouse - buttonBar->getPosition();
		}
	}
	else {
		mouseOffset = glm::vec2(-1.0f);
	}

	if (mouseOffset.x >= 0.0f) {
		float p = (input.mouse.y - mouseOffset.y) / (size.y - 2 * size.x - buttonBar->getSize().y);
		p = clamp(p, 0.0f, 1.0f);
		progress = (int)(p * (maxProgress - minProgress) + minProgress);
		buttonBar->setPosition(glm::vec2(0.0f, p*(size.y - 2 * size.x - buttonBar->getSize().y) + size.x));
	}
}

void GuiScrollBar::onDraw()
{
	super::onDraw();

	glm::vec2 position = glm::floor(getAbsolutePosition());

	glm::vec2 texSize = gui.getRenderer()->getTextureSize(texture);

	gui.getRenderer()->render(
		texture,
		(texSize / 3.0f) * glm::vec2(0.0f, 0.0f),
		(texSize / 3.0f),
		position,
		texSize / 3.0f);
	gui.getRenderer()->render(
		texture,
		(texSize / 3.0f) * glm::vec2(1.0f, 0.0f),
		(texSize / 3.0f),
		position + (texSize / 3.0f) * glm::vec2(1.0f, 0.0f),
		glm::vec2(getSize().x - texSize.x*2.0f / 3.0f, texSize.y / 3.0f));
	gui.getRenderer()->render(
		texture,
		(texSize / 3.0f) * glm::vec2(2.0f, 0.0f),
		(texSize / 3.0f),
		position + glm::vec2(getSize().x - texSize.x / 3.0f, 0.0f),
		texSize / 3.0f);
	gui.getRenderer()->render(
		texture,
		(texSize / 3.0f) * glm::vec2(0.0f, 1.0f),
		(texSize / 3.0f),
		position + glm::vec2(0.0f, texSize.y / 3.0f),
		glm::vec2(texSize.x / 3.0f, getSize().y - 2.0f * texSize.y / 3.0f));
	gui.getRenderer()->render(
		texture,
		(texSize / 3.0f) * glm::vec2(1.0f, 1.0f),
		(texSize / 3.0f),
		position + texSize / 3.0f,
		getSize() - 2.0f * texSize / 3.0f);
	gui.getRenderer()->render(
		texture,
		(texSize / 3.0f) * glm::vec2(2.0f, 1.0f),
		(texSize / 3.0f),
		position + glm::vec2(getSize().x - texSize.x / 3.0f, texSize.y / 3.0f),
		glm::vec2(texSize.x / 3.0f, getSize().y - 2.0f * texSize.y / 3.0f));
	gui.getRenderer()->render(
		texture,
		(texSize / 3.0f) * glm::vec2(0.0f, 2.0f),
		(texSize / 3.0f),
		position + glm::vec2(0.0f, getSize().y - texSize.y / 3.0f),
		texSize / 3.0f);
	gui.getRenderer()->render(
		texture,
		(texSize / 3.0f) * glm::vec2(1.0f, 2.0f),
		(texSize / 3.0f),
		position + glm::vec2(texSize.x / 3.0f, getSize().y - texSize.y / 3.0f),
		glm::vec2(getSize().x - 2.0f * texSize.x / 3.0f, texSize.y / 3.0f));
	gui.getRenderer()->render(
		texture,
		(texSize / 3.0f) * glm::vec2(2.0f, 2.0f),
		(texSize / 3.0f),
		position + getSize() - texSize / 3.0f,
		texSize / 3.0f);
}

void GuiScrollBar::setButtonUp(GuiButton *b)
{
	buttonUp = b;
}
void GuiScrollBar::setButtonDown(GuiButton *b)
{
	buttonDown = b;
}
void GuiScrollBar::setButtonBar(GuiButton *b)
{
	buttonBar = b;
}
void GuiScrollBar::setTexture(GuiTexture *t)
{
	texture = t;
}
void GuiScrollBar::setSize(glm::vec2 s)
{
	super::setSize(s);

	buttonUp->setSize(glm::vec2(s.x, s.x));
	buttonDown->setPosition(glm::vec2(0.0f, s.y - s.x));
	buttonDown->setSize(glm::vec2(s.x, s.x));
	buttonBar->setPosition(glm::vec2(0.0f, s.x));
	buttonBar->setSize(glm::vec2(s.x, (s.y - 2.0f*s.x) / 2.0f));
}

GuiButton *GuiScrollBar::getButtonUp()
{
	return buttonUp;
}
GuiButton *GuiScrollBar::getButtonDown()
{
	return buttonDown;
}
GuiButton *GuiScrollBar::getButtonBar()
{
	return buttonBar;
}
GuiTexture *GuiScrollBar::getTexture()
{
	return texture;
}

int GuiScrollBar::getProgress()
{
	return progress;
}

SerializedData GuiScrollBar::Serialize()
{
	SerializedData data;
	data.Push(super::Serialize());
	data.Push(buttonUp->Serialize());
	data.Push(buttonDown->Serialize());
	data.Push(buttonBar->Serialize());
	data.Push(gui.getTexturePath(texture));
	data.Push(minProgress);
	data.Push(maxProgress);
	return data;
}

void GuiScrollBar::Deserialize(SerializedData & data)
{
	super::Deserialize(data);
	buttonUp->Deserialize(data);
	buttonDown->Deserialize(data);
	buttonBar->Deserialize(data);
	string str;
	data.Pop(str);
	texture = gui.getTexture(str);
	data.Pop(minProgress);
	data.Pop(maxProgress);
}

void GuiScrollBar::setDefaultButtonUp(GuiButton *b)
{
	defaultButtonUp = b;
}
void GuiScrollBar::setDefaultButtonDown(GuiButton *b)
{
	defaultButtonDown = b;
}
void GuiScrollBar::setDefaultButtonBar(GuiButton *b)
{
	defaultButtonBar = b;
}
void GuiScrollBar::setDefaultTexture(GuiTexture *t)
{
	defaultTexture = t;
}
GuiButton *GuiScrollBar::getDefaultButtonUp()
{
	return defaultButtonUp;
}
GuiButton *GuiScrollBar::getDefaultButtonDown()
{
	return defaultButtonDown;
}
GuiButton *GuiScrollBar::getDefaultButtonBar()
{
	return defaultButtonBar;
}
GuiTexture *GuiScrollBar::getDefaultTexture()
{
	return defaultTexture;
}