#include "GuiTreeViewLayoutNode.h"

#include "GuiManager.h"

GuiTreeViewLayoutNode::GuiTreeViewLayoutNode(GuiLayout *layout)
: GuiTreeViewNode(glm::vec2(0.0f))
{
	this->layout = layout;
	getText()->setText(layout->getLabel());
	setLabel("gui_treeviewlayout");
}
GuiTreeViewLayoutNode::GuiTreeViewLayoutNode(glm::vec2 p)
: GuiTreeViewNode(p)
, layout(0)
{

}
GuiTreeViewLayoutNode::GuiTreeViewLayoutNode(GuiTreeViewLayoutNode &c)
: GuiTreeViewNode(c)
, layout(c.layout)
{

}
GuiTreeViewLayoutNode::~GuiTreeViewLayoutNode()
{

}

void GuiTreeViewLayoutNode::onInput()
{
	super::onInput();

	if (input.isButtonUp(SDL_BUTTON_LEFT))
	if (layout)
	{
		if (this->isSelected())
			layout->setSelected(true);
	}
}

void GuiTreeViewLayoutNode::setLayout(GuiLayout *layout)
{
	this->layout = layout;
}

GuiLayout *GuiTreeViewLayoutNode::getLayout()
{
	return layout;
}

GuiTreeViewNode *GuiTreeViewLayoutNode::addNode(GuiLayout *layout)
{
	GuiTreeViewLayoutNode *node = new GuiTreeViewLayoutNode(layout);
	if (layout)
		node->getText()->setText(layout->getLabel());
	return GuiTreeViewNode::addNode(node);
}

GuiTreeViewLayoutNode *GuiTreeViewLayoutNode::findByLayout(GuiLayout *layout)
{
	if (this->layout == layout)
		return this;
	else
	{
		GuiTreeViewLayoutNode *node = 0;
		for (unsigned int i = 0; i < getNumNodes(); i++)
		{
			node = dynamic_cast<GuiTreeViewLayoutNode*>(getNode(i))->findByLayout(layout);
			if (node)
				break;
		}
		return node;
	}
}
