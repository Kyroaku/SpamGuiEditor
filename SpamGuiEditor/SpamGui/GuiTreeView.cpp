#include "GuiTreeView.h"

#include "GuiManager.h"

GuiTexture *GuiTreeView::defaultTexture = 0;

GuiTreeView::GuiTreeView(glm::vec2 p, glm::vec2 s)
: GuiLayout(p, s)
, texture(defaultTexture)
, multiselect(true)
{
	setID("GuiTreeView");

	setLabel("treeview");
}
GuiTreeView::GuiTreeView(GuiTreeView &c)
: GuiLayout(c)
, multiselect(c.multiselect)
, texture(c.texture)
{
	for (unsigned int i = 0; i < c.getNumNodes(); i++)
		addNode(new GuiTreeViewNode(*c.getNode(i)));
}
GuiTreeView::~GuiTreeView()
{

}

void GuiTreeView::onInput()
{
	super::onInput();
}
void GuiTreeView::onUpdate(float dt)
{
	super::onUpdate(dt);

	selectedNodes.clear();
	glm::vec2 pos = glm::vec2(0.0f);
	for (unsigned int i = 0; i < nodes.size(); i++)
		updateNode(nodes.at(i), dt, pos);
}
void GuiTreeView::onDraw()
{
	super::onDraw();

	glm::vec2 position = glm::floor(getAbsolutePosition());

	glm::vec2 texSize = gui.getRenderer()->getTextureSize(texture);

	gui.getRenderer()->render(
		texture,
		(texSize / 3.0f) * glm::vec2(0.0f, 0.0f),
		(texSize / 3.0f),
		position,
		texSize / 3.0f);
	gui.getRenderer()->render(
		texture,
		(texSize / 3.0f) * glm::vec2(1.0f, 0.0f),
		(texSize / 3.0f),
		position + (texSize / 3.0f) * glm::vec2(1.0f, 0.0f),
		glm::vec2(getSize().x - texSize.x*2.0f / 3.0f, texSize.y / 3.0f));
	gui.getRenderer()->render(
		texture,
		(texSize / 3.0f) * glm::vec2(2.0f, 0.0f),
		(texSize / 3.0f),
		position + glm::vec2(getSize().x - texSize.x / 3.0f, 0.0f),
		texSize / 3.0f);
	gui.getRenderer()->render(
		texture,
		(texSize / 3.0f) * glm::vec2(0.0f, 1.0f),
		(texSize / 3.0f),
		position + glm::vec2(0.0f, texSize.y / 3.0f),
		glm::vec2(texSize.x / 3.0f, getSize().y - 2.0f * texSize.y / 3.0f));
	gui.getRenderer()->render(
		texture,
		(texSize / 3.0f) * glm::vec2(1.0f, 1.0f),
		(texSize / 3.0f),
		position + texSize / 3.0f,
		getSize() - 2.0f * texSize / 3.0f);
	gui.getRenderer()->render(
		texture,
		(texSize / 3.0f) * glm::vec2(2.0f, 1.0f),
		(texSize / 3.0f),
		position + glm::vec2(getSize().x - texSize.x / 3.0f, texSize.y / 3.0f),
		glm::vec2(texSize.x / 3.0f, getSize().y - 2.0f * texSize.y / 3.0f));
	gui.getRenderer()->render(
		texture,
		(texSize / 3.0f) * glm::vec2(0.0f, 2.0f),
		(texSize / 3.0f),
		position + glm::vec2(0.0f, getSize().y - texSize.y / 3.0f),
		texSize / 3.0f);
	gui.getRenderer()->render(
		texture,
		(texSize / 3.0f) * glm::vec2(1.0f, 2.0f),
		(texSize / 3.0f),
		position + glm::vec2(texSize.x / 3.0f, getSize().y - texSize.y / 3.0f),
		glm::vec2(getSize().x - 2.0f * texSize.x / 3.0f, texSize.y / 3.0f));
	gui.getRenderer()->render(
		texture,
		(texSize / 3.0f) * glm::vec2(2.0f, 2.0f),
		(texSize / 3.0f),
		position + getSize() - texSize / 3.0f,
		texSize / 3.0f);
}

void GuiTreeView::setMultiselect(bool s)
{
	multiselect = s;
}

unsigned int GuiTreeView::getNumNodes()
{
	return nodes.size();
}
std::vector<GuiTreeViewNode*> GuiTreeView::getSelectedNodes()
{
	return selectedNodes;
}
GuiTreeViewNode *GuiTreeView::getNode(unsigned int i)
{
	return nodes.at(i);
}

GuiTreeViewNode *GuiTreeView::addNode(GuiTreeViewNode *node)
{
	if (!node)
		return 0;
	addLayout(node);
	nodes.push_back(node);
	node->setTree(this);
	node->setSize(node->getText()->getSize() + node->getText()->getPosition());
	return node;
}
GuiTreeViewNode *GuiTreeView::addNode(std::string text)
{
	GuiTreeViewNode *node = new GuiTreeViewNode();
	node->getText()->setText(text);
	return addNode(node);
}

SerializedData GuiTreeView::Serialize()
{
	SerializedData data;
	data.Push(super::Serialize());
	data.Push(gui.getTexturePath(texture));
	data.Push(nodes.size());
	for (auto node : nodes)
		data.Push(node->Serialize());
	data.Push(multiselect);
	return data;
}

void GuiTreeView::Deserialize(SerializedData & data)
{
	super::Deserialize(data);
	string str;
	data.Pop(str);
	texture = gui.getTexture(str);
	size_t size;
	data.Pop(size);
	for (size_t i = 0; i < size; i++) {
		nodes.push_back(new GuiTreeViewNode);
		nodes.back()->Deserialize(data);
		nodes.back()->setTree(this);
	}
	data.Pop(multiselect);
}

bool GuiTreeView::isMultiselect()
{
	return multiselect;
}

void GuiTreeView::updateNode(GuiTreeViewNode *node, float dt, glm::vec2 &pos)
{
	if (!node)
		return;

	node->setPosition(pos);

	if (pos.y > getSize().y || pos.y < 0)
		node->setEnabled(false);
	else
		node->setEnabled(true);

	if (node->isSelected())
		selectedNodes.push_back(node);

	/* Move position */
	pos.y += node->getSize().y + 1.0f;

	/* Return, if subnodes are not visible */
	if (!node->isOpen())
		return;

	/* Move position and update subnodes */
	pos.x += 15.f;
	for (unsigned int i = 0; i < node->getNumNodes(); i++)
	if (node->getNode(i)->isEnabled())
		updateNode(node->getNode(i), dt, pos);
	pos.x -= 15.f;
}

void GuiTreeView::setDefaultTexture(GuiTexture *t)
{
	defaultTexture = t;
}
GuiTexture *GuiTreeView::getDefaultTexture()
{
	return defaultTexture;
}