#include "GuiButton.h"

#include "GuiManager.h"

GuiTexture *GuiButton::defaultTexture = 0;
GuiText *GuiButton::defaultText = 0;

GuiButton::GuiButton(std::string text, glm::vec2 p, glm::vec2 s) :
GuiButton(defaultTexture, text, p, s)
{

}
GuiButton::GuiButton(GuiTexture *t, std::string text, glm::vec2 p, glm::vec2 s)
: GuiLayout(p, s)
, texture(t)
, pressed(false)
, clicked(false)
{
	setID("GuiButton");

	setLabel("button");

	addLayout(this->text = new GuiText(*defaultText));
	this->text->setText(text);
	this->text->setBelongsToParent(true);

	setSize(getSize());
}
GuiButton::GuiButton(const GuiButton &c)
: GuiLayout(c)
, texture(c.texture)
, pressed(c.pressed)
, clicked(c.clicked)
{
	addLayout(this->text = new GuiText(*c.text));
	this->text->setText(c.text->getText());
}

bool GuiButton::isPressed()
{
	return pressed;
}
bool GuiButton::isClicked()
{
	bool c = clicked;
	clicked = false;
	return c;
}

void GuiButton::setSize(glm::vec2 s)
{
	glm::vec2 newSize;
	if (s.x < 0.0f)
		newSize.x = text->getSize().x + 15.0f;
	else
		newSize.x = s.x;
	if (s.y < 0.0f)
		newSize.y = text->getSize().y + 15.0f;
	else
		newSize.y = s.y;

	super::setSize(newSize);
}

SerializedData GuiButton::Serialize()
{
	SerializedData data;
	data.Push(super::Serialize());
	data.Push(gui.getTexturePath(texture));
	data.Push(text->Serialize());
	return data;
}

void GuiButton::Deserialize(SerializedData & data)
{
	super::Deserialize(data);
	string path;
	data.Pop(path);
	texture = gui.getTexture(path);
	text->Deserialize(data);
}

void GuiButton::setTexture(GuiTexture *texture)
{
	this->texture = texture;
	if (texture)
		setSize(gui.getRenderer()->getTextureSize(texture));
}
GuiTexture *GuiButton::getTexture()
{
	return texture;
}
void GuiButton::setText(GuiText *t)
{
	if (text)
		delete text;
	text = t;
}
GuiText *GuiButton::getText()
{
	return text;
}

void GuiButton::onInput()
{
	super::onInput();

	if (input.isButtonDown(SDL_BUTTON_LEFT)
		&& gui.isPointInRect(input.mouse, getAbsolutePosition(), getSize()))
		clicked = true;

	if (input.isButton(SDL_BUTTON_LEFT))
		pressed = true;
	else
		pressed = false;
}
void GuiButton::onUpdate(float dt)
{
	super::onUpdate(dt);


}
void GuiButton::onDraw()
{
	super::onDraw();

	glm::vec2 position = glm::floor(getAbsolutePosition());

	glm::vec2 texSize = gui.getRenderer()->getTextureSize(texture);

	gui.getRenderer()->render(
		texture,
		(texSize / 3.0f) * glm::vec2(0.0f, 0.0f),
		(texSize / 3.0f),
		position,
		texSize / 3.0f);
	gui.getRenderer()->render(
		texture,
		(texSize / 3.0f) * glm::vec2(1.0f, 0.0f),
		(texSize / 3.0f),
		position + (texSize / 3.0f) * glm::vec2(1.0f, 0.0f),
		glm::vec2(getSize().x - texSize.x*2.0f / 3.0f, texSize.y / 3.0f));
	gui.getRenderer()->render(
		texture,
		(texSize / 3.0f) * glm::vec2(2.0f, 0.0f),
		(texSize / 3.0f),
		position + glm::vec2(getSize().x - texSize.x / 3.0f, 0.0f),
		texSize / 3.0f);
	gui.getRenderer()->render(
		texture,
		(texSize / 3.0f) * glm::vec2(0.0f, 1.0f),
		(texSize / 3.0f),
		position + glm::vec2(0.0f, texSize.y / 3.0f),
		glm::vec2(texSize.x / 3.0f, getSize().y - 2.0f * texSize.y / 3.0f));
	gui.getRenderer()->render(
		texture,
		(texSize / 3.0f) * glm::vec2(1.0f, 1.0f),
		(texSize / 3.0f),
		position + texSize / 3.0f,
		getSize() - 2.0f * texSize / 3.0f);
	gui.getRenderer()->render(
		texture,
		(texSize / 3.0f) * glm::vec2(2.0f, 1.0f),
		(texSize / 3.0f),
		position + glm::vec2(getSize().x - texSize.x / 3.0f, texSize.y / 3.0f),
		glm::vec2(texSize.x / 3.0f, getSize().y - 2.0f * texSize.y / 3.0f));
	gui.getRenderer()->render(
		texture,
		(texSize / 3.0f) * glm::vec2(0.0f, 2.0f),
		(texSize / 3.0f),
		position + glm::vec2(0.0f, getSize().y - texSize.y / 3.0f),
		texSize / 3.0f);
	gui.getRenderer()->render(
		texture,
		(texSize / 3.0f) * glm::vec2(1.0f, 2.0f),
		(texSize / 3.0f),
		position + glm::vec2(texSize.x / 3.0f, getSize().y - texSize.y / 3.0f),
		glm::vec2(getSize().x - 2.0f * texSize.x / 3.0f, texSize.y / 3.0f));
	gui.getRenderer()->render(
		texture,
		(texSize / 3.0f) * glm::vec2(2.0f, 2.0f),
		(texSize / 3.0f),
		position + getSize() - texSize / 3.0f,
		texSize / 3.0f);
}

void GuiButton::setDefaultTexture(GuiTexture *texture)
{
	defaultTexture = texture;
}
GuiTexture *GuiButton::getDefaultTexture()
{
	return defaultTexture;
}
void GuiButton::setDefaultText(GuiText *text)
{
	defaultText = text;
}
GuiText *GuiButton::getDefaultText()
{
	return defaultText;
}