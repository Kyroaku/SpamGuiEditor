#include "GuiRadioButton.h"

#include "GuiManager.h"

GuiButton *GuiRadioButton::defaultButtonCheck = 0;
GuiText *GuiRadioButton::defaultText = 0;
GuiTexture *GuiRadioButton::defaultTextureChecked = 0;
GuiTexture *GuiRadioButton::defaultTextureUnchecked = 0;

void GuiRadioButton::updateButtonCheck()
{
	if (checked)
		buttonCheck->setTexture(textureChecked);
	else
		buttonCheck->setTexture(textureUnchecked);
}

GuiRadioButton::GuiRadioButton()
	: GuiLayout()
	, checked(false)
	, textureChecked(defaultTextureChecked)
	, textureUnchecked(defaultTextureUnchecked)
{
	setID("GuiRadioButton");

	addLayout(buttonCheck = new GuiButton(*defaultButtonCheck));
	addLayout(text = new GuiText(*defaultText));

	buttonCheck->setFocusable(false);
	buttonCheck->setBelongsToParent(true);
	buttonCheck->setSize(glm::vec2(-1.0f));
	text->setFocusable(false);
	text->setBelongsToParent(true);

	setChecked(false);

	text->setPosition(glm::vec2(buttonCheck->getSize().x, 0.0f));
}

void GuiRadioButton::onInput()
{
	super::onInput();

	if (input.isButtonDown(SDL_BUTTON_LEFT)) {
		toggleChecked();
	}
}

void GuiRadioButton::onUpdate(float dt)
{
	super::onUpdate(dt);
}

void GuiRadioButton::onDraw()
{
	super::onDraw();
}

GuiButton * GuiRadioButton::getCheckButton()
{
	return buttonCheck;
}

GuiText * GuiRadioButton::getText()
{
	return text;
}

GuiTexture * GuiRadioButton::getTextureChecked()
{
	return textureChecked;
}

GuiTexture * GuiRadioButton::getTextureUnchecked()
{
	return textureUnchecked;
}

void GuiRadioButton::setTextureChecked(GuiTexture * t)
{
	textureChecked = t;
}

void GuiRadioButton::setTextureUnchecked(GuiTexture * t)
{
	textureUnchecked = t;
}

bool GuiRadioButton::isChecked()
{
	return checked;
}

void GuiRadioButton::setChecked(bool b)
{
	/* Uncheck all parent's radio buttons */
	if (getParent() && b)
		for (size_t i = 0; i < getParent()->getNumChildrens(); i++) {
			GuiRadioButton *layout = dynamic_cast<GuiRadioButton*>(getParent()->getChildren(i));
			if (layout && layout != this)
				layout->setChecked(false);
		}
	checked = b;
	updateButtonCheck();
}

void GuiRadioButton::toggleChecked()
{
	setChecked(checked ? false : true);
}

SerializedData GuiRadioButton::Serialize()
{
	SerializedData data;
	data.Push(super::Serialize());
	data.Push(buttonCheck->Serialize());
	data.Push(text->Serialize());
	data.Push(gui.getTexturePath(textureChecked));
	data.Push(gui.getTexturePath(textureUnchecked));
	return data;
}

void GuiRadioButton::Deserialize(SerializedData & data)
{
	super::Deserialize(data);
	buttonCheck->Deserialize(data);
	text->Deserialize(data);
	string str;
	data.Pop(str);
	textureChecked = gui.getTexture(str);
	data.Pop(str);
	textureUnchecked = gui.getTexture(str);
}

GuiButton * GuiRadioButton::getDefaultButtonCheck()
{
	return defaultButtonCheck;
}

GuiText * GuiRadioButton::getDefaultText()
{
	return defaultText;
}

GuiTexture * GuiRadioButton::getDefaultTextureChecked()
{
	return defaultTextureChecked;
}

GuiTexture * GuiRadioButton::getDefaultTextureUnchecked()
{
	return defaultTextureUnchecked;
}

void GuiRadioButton::setDefaultButtonCheck(GuiButton * button)
{
	defaultButtonCheck = button;
}

void GuiRadioButton::setDefaultText(GuiText * text)
{
	defaultText = text;
}

void GuiRadioButton::setDefaultTextureChecked(GuiTexture * texture)
{
	defaultTextureChecked = texture;
}

void GuiRadioButton::setDefaultTextureUnchecked(GuiTexture * texture)
{
	defaultTextureUnchecked = texture;
}
