#pragma once

#include "GuiText.h"

class GuiEditBox : public GuiLayout
{
	GuiTexture *texture;
	GuiText *text;
	bool typing;

	static GuiTexture *defaultTexture;
	static GuiText *defaultText;

public:
	GuiEditBox(glm::vec2 p = glm::vec2(0.0f), glm::vec2 s = glm::vec2(0.0f));
	GuiEditBox(const GuiEditBox &c);

	virtual void onInput();
	virtual void onUpdate(float dt);
	virtual void onDraw();

	virtual void setTexture(GuiTexture *t);
	virtual void setText(GuiText *t);

	virtual GuiTexture *getTexture();
	virtual GuiText *getText();

	virtual bool isTyping();

	virtual SerializedData Serialize();
	virtual void Deserialize(SerializedData &data);

	static void setDefaultTexture(GuiTexture *t);
	static GuiTexture *getDefaultTexture();
	static void setDefaultText(GuiText *t);
	static GuiText *getDefaultText();
};