#include "GuiEditBox.h"

#include "GuiManager.h"

GuiTexture *GuiEditBox::defaultTexture = 0;
GuiText *GuiEditBox::defaultText = 0;

GuiEditBox::GuiEditBox(glm::vec2 p, glm::vec2 s)
: GuiLayout(p, s)
, texture(defaultTexture)
, typing(false)
{
	setID("GuiEditBox");

	addLayout(text = new GuiText(*defaultText));
	text->setBelongsToParent(true);

	setLabel("editbox");
}
GuiEditBox::GuiEditBox(const GuiEditBox &c)
: GuiLayout(c)
, texture(c.texture)
, typing(false)
{
	addLayout(text = new GuiText(*c.text));
}

void GuiEditBox::onInput()
{
	super::onInput();

	if (input.isButtonDown(SDL_BUTTON_LEFT))
	if (gui.isPointInRect(input.mouse, getAbsolutePosition(), getSize()))
		typing = true;

	if (input.isAnyKeyDown())
	if (typing)
	{
		std::string t = text->getText();
		if (input.isKeyDown(SDL_SCANCODE_BACKSPACE))
		{
			if (t.length() > 0)
				t.pop_back();
		}
		else
		{
			t.push_back((char)input.event.key.keysym.sym);
		}
		text->setText(t);
	}
}
void GuiEditBox::onUpdate(float dt)
{
	super::onUpdate(dt);

	if (input.isButtonDown(SDL_BUTTON_LEFT))
	if (!gui.isPointInRect(input.mouse, getAbsolutePosition(), getSize()))
		typing = false;
}
void GuiEditBox::onDraw()
{
	super::onDraw();

	glm::vec2 position = glm::floor(getAbsolutePosition());

	glm::vec2 texSize = gui.getRenderer()->getTextureSize(texture);

	gui.getRenderer()->render(
		texture,
		(texSize / 3.0f) * glm::vec2(0.0f, 0.0f),
		(texSize / 3.0f),
		position,
		texSize / 3.0f);
	gui.getRenderer()->render(
		texture,
		(texSize / 3.0f) * glm::vec2(1.0f, 0.0f),
		(texSize / 3.0f),
		position + (texSize / 3.0f) * glm::vec2(1.0f, 0.0f),
		glm::vec2(getSize().x - texSize.x*2.0f / 3.0f, texSize.y / 3.0f));
	gui.getRenderer()->render(
		texture,
		(texSize / 3.0f) * glm::vec2(2.0f, 0.0f),
		(texSize / 3.0f),
		position + glm::vec2(getSize().x - texSize.x / 3.0f, 0.0f),
		texSize / 3.0f);
	gui.getRenderer()->render(
		texture,
		(texSize / 3.0f) * glm::vec2(0.0f, 1.0f),
		(texSize / 3.0f),
		position + glm::vec2(0.0f, texSize.y / 3.0f),
		glm::vec2(texSize.x / 3.0f, getSize().y - 2.0f * texSize.y / 3.0f));
	gui.getRenderer()->render(
		texture,
		(texSize / 3.0f) * glm::vec2(1.0f, 1.0f),
		(texSize / 3.0f),
		position + texSize / 3.0f,
		getSize() - 2.0f * texSize / 3.0f);
	gui.getRenderer()->render(
		texture,
		(texSize / 3.0f) * glm::vec2(2.0f, 1.0f),
		(texSize / 3.0f),
		position + glm::vec2(getSize().x - texSize.x / 3.0f, texSize.y / 3.0f),
		glm::vec2(texSize.x / 3.0f, getSize().y - 2.0f * texSize.y / 3.0f));
	gui.getRenderer()->render(
		texture,
		(texSize / 3.0f) * glm::vec2(0.0f, 2.0f),
		(texSize / 3.0f),
		position + glm::vec2(0.0f, getSize().y - texSize.y / 3.0f),
		texSize / 3.0f);
	gui.getRenderer()->render(
		texture,
		(texSize / 3.0f) * glm::vec2(1.0f, 2.0f),
		(texSize / 3.0f),
		position + glm::vec2(texSize.x / 3.0f, getSize().y - texSize.y / 3.0f),
		glm::vec2(getSize().x - 2.0f * texSize.x / 3.0f, texSize.y / 3.0f));
	gui.getRenderer()->render(
		texture,
		(texSize / 3.0f) * glm::vec2(2.0f, 2.0f),
		(texSize / 3.0f),
		position + getSize() - texSize / 3.0f,
		texSize / 3.0f);
}

void GuiEditBox::setTexture(GuiTexture *t)
{
	texture = t;
}
GuiTexture *GuiEditBox::getTexture()
{
	return texture;
}
void GuiEditBox::setText(GuiText *t)
{
	text = t;
}
GuiText *GuiEditBox::getText()
{
	return text;
}
bool GuiEditBox::isTyping()
{
	return typing;
}

SerializedData GuiEditBox::Serialize()
{
	SerializedData data;
	data.Push(super::Serialize());
	data.Push(gui.getTexturePath(texture));
	data.Push(text->Serialize());
	return data;
}

void GuiEditBox::Deserialize(SerializedData & data)
{
	super::Deserialize(data);
	string str;
	data.Pop(str);
	texture = gui.getTexture(str);
	text->Deserialize(data);
}

void GuiEditBox::setDefaultTexture(GuiTexture *t)
{
	defaultTexture = t;
}
GuiTexture *GuiEditBox::getDefaultTexture()
{
	return defaultTexture;
}
void GuiEditBox::setDefaultText(GuiText *t)
{
	defaultText = t;
}
GuiText *GuiEditBox::getDefaultText()
{
	return 	defaultText;
}