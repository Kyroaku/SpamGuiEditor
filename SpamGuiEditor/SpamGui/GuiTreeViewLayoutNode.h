#pragma once

#include "GuiTreeViewNode.h"

class GuiTreeViewLayoutNode : public GuiTreeViewNode
{
	GuiLayout *layout;

public:
	GuiTreeViewLayoutNode(GuiLayout *layout);
	GuiTreeViewLayoutNode(glm::vec2 p = glm::vec2(0.0f));
	GuiTreeViewLayoutNode(GuiTreeViewLayoutNode &c);
	~GuiTreeViewLayoutNode();

	virtual void onInput();

	virtual void setLayout(GuiLayout *layout);

	virtual GuiLayout *getLayout();

	virtual GuiTreeViewNode *addNode(GuiLayout *layout);

	virtual GuiTreeViewLayoutNode *findByLayout(GuiLayout *layout);

};