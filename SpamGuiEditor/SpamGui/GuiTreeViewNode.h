#pragma once

#include <vector>

#include "GuiButton.h"

class GuiTreeView;

class GuiTreeViewNode : public GuiLayout
{
	std::vector<GuiTreeViewNode*>nodes;

	GuiTexture *textureSelected;
	GuiButton *buttonOpen;
	GuiText *text;

	GuiTreeView *tree;
	GuiTreeViewNode *nodeParent;
	bool open, selected;

	static GuiTexture *defaultTextureSelected;
	static GuiButton *defaultButtonOpen;
	static GuiText *defaultText;

public:
	GuiTreeViewNode(glm::vec2 p = glm::vec2(0.0f));
	GuiTreeViewNode(GuiTreeViewNode &c);
	~GuiTreeViewNode();

	virtual void onInput();
	virtual void onUpdate(float dt);
	virtual void onDraw();

	virtual void setOpen(bool b);
	virtual void setEnabled(bool b);
	virtual void setSelected(bool b);
	virtual void setText(GuiText *t);
	virtual void setTree(GuiTreeView *t);
	virtual void setNodeParent(GuiTreeViewNode *p);
	virtual void setButtonOpen(GuiButton *b);

	virtual GuiTreeViewNode *getNodeParent();
	virtual GuiTreeView *getTree();
	virtual GuiButton *getButtonOpen();
	virtual GuiText *getText();
	virtual unsigned int getNumNodes();

	virtual bool isOpen();
	virtual bool isSelected();

	virtual GuiTreeViewNode *addNode(GuiTreeViewNode *node);
	virtual GuiTreeViewNode *addNode(std::string text);
	virtual GuiTreeViewNode *getNode(unsigned int i);

	virtual SerializedData Serialize();
	virtual void Deserialize(SerializedData &data);

	static void setDefaultTextureSelected(GuiTexture *t);
	static void setDefaultButtonOpen(GuiButton *b);
	static void setDefaultText(GuiText *t);
	static GuiTexture *getDefaultTextureSelected();
	static GuiButton *getDefaultButtonOpen();
	static GuiText *getDefaultText();
};