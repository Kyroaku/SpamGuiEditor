#include "GuiText.h"

#include "GuiManager.h"

const float GuiText::JustifyLeft = 0.0f;
const float GuiText::JustifyRight = 1.0f;
const float GuiText::JustifyTop = 0.0f;
const float GuiText::JustifyBottom = 1.0f;
const float GuiText::JustifyCenter = 0.5f;

GuiFont *GuiText::defaultFont = 0;

GuiText::GuiText(std::string t, glm::vec2 p) :
GuiText(defaultFont, t, p)
{

}
GuiText::GuiText(GuiFont *f, std::string t, glm::vec2 p)
: GuiLayout(p, glm::vec2(0.0f))
, font(f)
, color(glm::vec4(255, 255, 255, 0))
, adjustMode(eAdjustMode::HorizontalAndVertical)
, wrapMode(eWrapMode::None)
, justify(GuiText::JustifyLeft, GuiText::JustifyTop)
{
	setID("GuiText");
	setLabel("text");

	setFocusable(false);

	//texture = gui.getRenderer()->createTexture(s);
	setText(t);
}
GuiText::GuiText(GuiText &c)
: GuiLayout(c)
, font(c.font)
, color(c.color)
, lines(c.lines)
, lineWidths(c.lineWidths)
, adjustMode(c.adjustMode)
, wrapMode(c.wrapMode)
, justify(c.justify)
{
	setLabel("text");

	setFocusable(false);

	texture = gui.getRenderer()->createTexture(getSize());
	setText(c.text);
}

void GuiText::onDraw()
{
	super::onDraw();

	//renderText();
	gui.getRenderer()->render(texture, getAbsolutePosition(), getSize());
}

void GuiText::setFont(GuiFont *f)
{
	font = f;
}
void GuiText::setText(std::string t)
{
	text = t;
	if (font) {
		wrapText();
		adjustLayoutSize();
		renderText();
	}
}
void GuiText::setColor(int r, int g, int b)
{
	setColor(glm::vec4(r, g, b, 0));
}
void GuiText::setColor(glm::vec4 c)
{
	color = c;
	renderText();
}
void GuiText::setAdjustMode(eAdjustMode am)
{
	adjustMode = am;
	adjustLayoutSize();
}
void GuiText::setWrapMode(eWrapMode wm)
{
	wrapMode = wm;
	wrapText();
}
void GuiText::setJustify(float x, float y)
{
	justify = glm::vec2(x, y);
}
void GuiText::setSize(glm::vec2 s)
{
	GuiLayout::setSize(s);

	gui.getRenderer()->destroyTexture(texture);
	texture = gui.getRenderer()->createTexture(s);
	renderText();
}

GuiFont *GuiText::getFont()
{
	return font;
}
std::string &GuiText::getText()
{
	return text;
}
glm::vec4 GuiText::getColor()
{
	return color;
}
eWrapMode GuiText::getWrapMode()
{
	return wrapMode;
}
glm::vec2 GuiText::getJustify()
{
	return justify;
}

bool GuiText::isAdjustMode(eAdjustMode am)
{
	return ((int)adjustMode & (int)am) ? true : false;
}

SerializedData GuiText::Serialize()
{
	SerializedData data;
	data.Push(super::Serialize());
	data.Push(gui.getFontPath(font));
	data.Push(text);
	data.Push(color);
	data.Push(adjustMode);
	data.Push(wrapMode);
	data.Push(justify);
	return data;
}

void GuiText::Deserialize(SerializedData & data)
{
	super::Deserialize(data);
	string str;
	data.Pop(str);
	font = gui.getFont(str);
	data.Pop(text);
	data.Pop(color);
	data.Pop(adjustMode);
	data.Pop(wrapMode);
	data.Pop(justify);
}

void GuiText::renderText()
{
	if (texture == 0)
		return;
	gui.getRenderer()->setRenderTarget(texture);
	gui.getRenderer()->clear(glm::ivec4(255, 0, 255, 0));

	for (unsigned int i = 0; i < lines.size(); i++)
		gui.getRenderer()->print(
		font,
		glm::vec2(
		getJustify().x*(getSize().x - lineWidths.at(i)),
		getJustify().y*(getSize().y - lines.size() * ((BFF_Font*)font)->getCellSize().y) + i * ((BFF_Font*)font)->getCellSize().y
		),
		lines.at(i),
		color
		);
	gui.getRenderer()->setRenderTarget(NULL);
}
void GuiText::adjustLayoutSize()
{
	if (isAdjustMode(eAdjustMode::None))
		return;

	glm::vec2 size = getSize();
	if (isAdjustMode(eAdjustMode::Horizontal))
	{
		size.x = 0.0f;
		/* find the longest line and set layout size */
		for (unsigned int i = 0; i < lineWidths.size(); i++)
		{
			if (lineWidths.at(i) > size.x)
				size.x = (float)lineWidths.at(i);
		}
	}
	if (isAdjustMode(eAdjustMode::Vertical))
		size.y = (float)lineWidths.size() * ((BFF_Font*)font)->getCellSize().y;

	setSize(size);
}
void GuiText::wrapText()
{
	if (wrapMode == eWrapMode::WrapToLayout)
		wrapTextToLayout(this);
	else
	if (wrapMode == eWrapMode::WrapToParent)
		wrapTextToLayout(getParent());
	else
		wrapTextToEnters();
}
void GuiText::wrapTextToEnters()
{
	lines.clear();
	lineWidths.clear();
	if (text.empty())
		return;
	lineWidths.push_back(0);
	std::string line = "";
	int lastNewLineIndex = 0;

	for (unsigned int i = 0; i < text.length(); i++)
	{
		/* wrap if new line character */
		if (text[i] == '\n') {
			/* push back text between two new line characters */
			lines.push_back(text.substr(lastNewLineIndex, i - lastNewLineIndex));
			lastNewLineIndex = i + 1;
			lineWidths.push_back(0);
			continue;
		}
		/* add character width to line width */
		lineWidths.at(lineWidths.size() - 1) += ((BFF_Font*)font)->getCharacterWidth(text[i]);
	}

	/* push back text from last new line character to the end of string */
	lines.push_back(text.substr(lastNewLineIndex, text.length() - lastNewLineIndex));
}
void GuiText::wrapTextToLayout(GuiLayout *layout)
{
	/* TODO: Implement */
}

void GuiText::setDefaultFont(GuiFont *font)
{
	defaultFont = font;
}
GuiFont *GuiText::getDefaultFont()
{
	return defaultFont;
}