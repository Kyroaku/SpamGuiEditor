#pragma once

class GuiLayout;

class IGuiFactory {
public:
	virtual GuiLayout *create() = 0;
	virtual GuiLayout *copy(GuiLayout *) = 0;
};

template<typename T> class GuiFactory : public IGuiFactory
{
public:
	virtual T *create() {
		return new T();
	}
	virtual T *copy(GuiLayout *c) {
		return new T(*(T*)c);
	}
};