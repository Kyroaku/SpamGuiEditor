#pragma once

#include "GuiCheckBox.h"

class GuiRadioButton : public GuiLayout {
	GuiButton *buttonCheck;
	GuiText *text;
	GuiTexture *textureChecked, *textureUnchecked;

	bool checked;

	static GuiButton *defaultButtonCheck;
	static GuiText *defaultText;
	static GuiTexture *defaultTextureChecked;
	static GuiTexture *defaultTextureUnchecked;

	void updateButtonCheck();

public:
	GuiRadioButton();

	virtual void onInput();
	virtual void onUpdate(float dt);
	virtual void onDraw();

	virtual GuiButton *getCheckButton();
	virtual GuiText *getText();
	virtual GuiTexture *getTextureChecked();
	virtual GuiTexture *getTextureUnchecked();

	virtual void setTextureChecked(GuiTexture *t);
	virtual void setTextureUnchecked(GuiTexture *t);

	virtual bool isChecked();
	virtual void setChecked(bool b);
	virtual void toggleChecked();

	virtual SerializedData Serialize();
	virtual void Deserialize(SerializedData &data);

	static GuiButton *getDefaultButtonCheck();
	static GuiText *getDefaultText();
	static GuiTexture *getDefaultTextureChecked();
	static GuiTexture *getDefaultTextureUnchecked();
	static void setDefaultButtonCheck(GuiButton *button);
	static void setDefaultText(GuiText *text);
	static void setDefaultTextureChecked(GuiTexture *texture);
	static void setDefaultTextureUnchecked(GuiTexture *texture);
};